function inge -d "El inge que ayuda a realizar pruebas automatizadas"
  set comando $argv[1]
  set singleClass false
  set singleMethod false
  set --erase argv[1]
  if test $comando
    switch $comando;
      case '-v';
        echo 'v0.0.1'
        return
      case 'ayuda';
        echo "Uso: inge <comando> <opciones> [Nombre de actividad] [Nombre de método]"
        echo ""
        echo "Comandos:"
        echo "ayuda : Muestra este menú de ayuda."
        echo "prueba : Inicia las pruebas para el directorio actual."
        echo ""
        echo "Opciones:"
        echo "dev: Prueba con configuración de desarrollo"
        echo "prod: Prueba con configuración de producción"
        echo "todo: Realiza todas las pruebas para todas las configuraciones"
        return
      case 'prueba';
        if test $argv[1]
          set opcion1 $argv[1]
          set --erase argv[1]
          if test $argv[1]
            set singleClass true
            set activityName $argv[1]
            set --erase argv[1]
            if test $argv[1]
              set singleMethod true
              set methodName $argv[1]
              set --erase argv[1]
            end
          end
          switch $opcion1;
            case 'todo';
              eval "./gradlew cAT"
              echo "Ya terminé de probar tooooooooodo. ¿Cómo lo vio Inge? ¿Necesita algo más?"
              return
            case 'dev';
              if eval $singleClass
                if eval $singleMethod
                  echo "Voy a hacer la prueba $methodName de $activityName en desarrollo."
                else
                  echo "Voy a empezar las pruebas de $activityName en desarrollo."
                end
              else
                echo "Voy a  empezar todas las pruebas en desarrollo."
              end
              echo ""
              echo ""
              if eval $singleClass
                if eval $singleMethod
                  eval "./gradlew connectedDevelopmentDebugAndroidTest -Pandroid.testInstrumentationRunnerArguments.class=com.bosc.TEVi.activities.$activityName#$methodName"
                else
                  eval "./gradlew connectedDevelopmentDebugAndroidTest -Pandroid.testInstrumentationRunnerArguments.class=com.bosc.TEVi.activities.$activityName"
                end
              else
                eval "./gradlew connectedDevelopmentDebugAndroidTest"
              end
              echo ""
              echo ""
              echo "Ya terminé de probar en desarrollo ¿Cómo lo vio Inge? ¿Necesita algo más?"
              return
            case 'prod';
              echo "Voy a  empezar las pruebas en producción."
              echo ""
              echo ""
              eval "./gradlew connectedProductionDebugAndroidTest"
              echo ""
              echo ""
              echo "Ya terminé las pruebas en producción ¿Cómo lo vio Inge? ¿Necesita algo más?"
              return
            case '*';
              echo "No le entendí Inge, ya se parece a mi"
              return
          end
          return
        else
          echo "¿Olvidó poner las opciones Inge? Para conocer más, pídame ayuda"
          return
        end        
        return
      case '*';
        echo "No me pida eso Inge, no lo entiendo, pídame ayuda y ayúdeme a ayudarlo."
        return
    end
  else
    echo "¿En qué lo apoyo Inge? Para conocer más, pídame ayuda"
  end
end