function live_merge
  git push
  git checkout staging
  git merge development
  git push
  git checkout live
  git merge staging
  git push
  git push live live:master
  git checkout development
end